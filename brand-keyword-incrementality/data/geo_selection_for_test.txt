geo	Metro	Strata	Assignment
501	New York NY	1	2
803	Los Angeles CA	1	1
807	San Francisco-Oakland-San Jose CA	2	2
602	Chicago IL	2	1
506	Boston MA-Manchester NH	3	1
623	Dallas-Ft. Worth TX	3	2
613	Minneapolis-St. Paul MN	4	1
618	Houston TX	4	2
751	Denver CO	5	1
511	Washington DC (Hagerstown MD)	5	2
819	Seattle-Tacoma WA	6	1
504	Philadelphia PA	6	2
505	Detroit MI	7	1
560	Raleigh-Durham (Fayetteville) NC	7	2
524	Atlanta GA	8	2
659	Nashville TN	8	1
820	Portland OR	9	1
630	Birmingham (Ann and Tusc) AL	9	2
617	Milwaukee WI	10	1
539	Tampa-St. Petersburg (Sarasota) FL	10	2
825	San Diego CA	11	2
862	Sacramento-Stockton-Modesto CA	11	1
533	Hartford & New Haven CT	12	2
753	Phoenix AZ	12	1
609	St. Louis MO	13	1
529	Louisville KY	13	2
517	Charlotte NC	14	1
508	Pittsburgh PA	14	2
534	Orlando-Daytona Beach-Melbourne FL	15	2
510	Cleveland-Akron (Canton) OH	15	1
557	Knoxville TN	16	2
616	Kansas City MO	16	1
650	Oklahoma City OK	17	1
512	Baltimore MD	17	2
648	Champaign & Springfield-Decatur IL	18	2
770	Salt Lake City UT	18	1
571	Ft. Myers-Naples FL	19	2
635	Austin TX	19	1
535	Columbus OH	20	1
693	Little Rock-Pine Bluff AR	20	2
527	Indianapolis IN	21	2
563	Grand Rapids-Kalamazoo-Battle Creek MI	21	1
544	Norfolk-Portsmouth-Newport News VA	22	1
622	New Orleans LA	22	2
675	Peoria-Bloomington IL	23	1
521	"Providence-New Bedford,MA"	23	2
686	Mobile AL-Pensacola (Ft. Walton Beach) FL	24	2
811	Reno NV	24	1
671	Tulsa OK	25	1
652	Omaha NE	25	2
518	Greensboro-High Point-Winston Salem NC	26	2
611	"Rochester-Mason City-Austin,IA"	26	1
528	Miami-Ft. Lauderdale FL	27	1
724	Fargo-Valley City ND	27	2
515	Cincinnati OH	28	2
679	Des Moines-Ames IA	28	1
500	Portland-Auburn ME	29	2
541	Lexington KY	29	1
542	Dayton OH	30	1
839	Las Vegas NV	30	2
530	Tallahassee FL-Thomasville GA	31	2
881	Spokane WA	31	1
744	Honolulu HI	32	1
561	Jacksonville FL	32	2
625	Waco-Temple-Bryan TX	33	1
640	Memphis TN	33	2
658	Green Bay-Appleton WI	34	1
641	San Antonio TX	34	2
669	Madison WI	35	2
722	Lincoln & Hastings-Kearney NE	35	1
548	West Palm Beach-Ft. Pierce FL	36	2
682	Davenport IA-Rock Island-Moline IL	36	1
670	Ft. Smith-Fayetteville-Springdale-Rogers AR	37	2
866	Fresno-Visalia CA	37	1
570	Florence-Myrtle Beach SC	38	2
718	Jackson MS	38	1
813	Medford-Klamath Falls OR	39	2
632	Paducah KY-Cape Girardeau MO-Harrisburg-Mount Vernon IL	39	1
540	Traverse City-Cadillac MI	40	2
752	Colorado Springs-Pueblo CO	40	1
633	Odessa-Midland TX	41	2
691	Huntsville-Decatur (Florence) AL	41	1
514	Buffalo NY	42	1
519	Charleston SC	42	2
762	Missoula MT	43	2
532	Albany-Schenectady-Troy NY	43	1
692	Beaumont-Port Arthur TX	44	2
790	Albuquerque-Santa Fe NM	44	1
546	Columbia SC	45	1
538	Rochester NY	45	2
513	Flint-Saginaw-Bay City MI	46	2
662	Abilene-Sweetwater TX	46	1
567	Greenville-Spartanburg-Asheville-Anderson	47	1
555	Syracuse NY	47	2
624	Sioux City IA	48	1
522	Columbus GA	48	2
789	Tucson (Sierra Vista) AZ	49	1
821	Bend OR	49	2
716	Baton Rouge LA	50	1
642	Lafayette LA	50	2
800	Bakersfield CA	51	1
503	Macon GA	51	2
678	Wichita-Hutchinson KS	52	2
556	Richmond-Petersburg VA	52	1
610	Rockford IL	53	1
1000	Palm Springs CA	53	2
507	Savannah GA	54	2
582	Lafayette IN	54	1
649	Evansville IN	55	2
523	Burlington VT-Plattsburgh NY	55	1
612	Shreveport LA	56	2
637	Cedar Rapids-Waterloo-Iowa City & Dubuque IA	56	1
509	Ft. Wayne IN	57	2
543	Springfield-Holyoke MA	57	1
550	Wilmington NC	58	1
516	Erie PA	58	2
573	Roanoke-Lynchburg VA	59	2
743	Anchorage AK	59	1
656	Panama City FL	60	2
619	Springfield MO	61	2
757	Boise ID	61	1
577	Wilkes Barre-Scranton PA	62	2
771	Yuma AZ-El Centro CA	60	1
810	Yakima-Pasco-Richland-Kennewick WA	62	1
547	Toledo OH	63	2
828	Monterey-Salinas CA	63	1
687	Minot-Bismarck-Dickinson(Williston) ND	64	1
520	Augusta GA	64	2
725	Sioux Falls(Mitchell) SD	65	1
765	El Paso TX	65	2
598	Clarksburg-Weston WV	66	1
855	Santa Barbara-Santa Maria-San Luis Obispo CA	66	2
710	Hattiesburg-Laurel MS	67	1
575	Chattanooga TN	67	2
758	Idaho Falls-Pocatello ID	68	2
564	Charleston-Huntington WV	68	1
545	Greenville-New Bern-Washington NC	69	2
566	Harrisburg-Lancaster-Lebanon-York PA	69	1
531	Tri-Cities TN-VA	70	2
676	Duluth MN-Superior WI	71	2
764	Rapid City SD	71	1
638	St. Joseph MO	70	1
754	Butte-Bozeman MT	72	2
604	Columbia-Jefferson City MO	72	1
628	Monroe LA-El Dorado AR	73	1
554	Wheeling WV-Steubenville OH	73	2
526	Utica NY	74	1
627	Wichita Falls TX & Lawton OK	74	2
756	"Billings, MT"	75	1
651	Lubbock TX	75	2
736	Bowling Green KY	76	2
644	Alexandria LA	76	1
705	Wausau-Rhinelander WI	77	2
502	Binghamton NY	77	1
747	Juneau AK	78	2
801	Eugene OR	78	1
553	Marquette MI	79	2
639	Jackson TN	79	1
605	Topeka KS	80	1
657	"Sherman-Ada, OK"	80	2
709	Tyler-Longview(Lufkin & Nacogdoches) TX	81	2
634	Amarillo TX	81	1
698	"Montgomery-Selma, AL"	82	2
584	Charlottesville VA	82	1
759	Cheyenne WY-Scottsbluff NE	83	2
755	Great Falls MT	83	1
536	Youngstown OH	84	2
574	Johnstown-Altoona-State College PA	84	1
767	Casper-Riverton WY	85	1
643	Lake Charles LA	85	2
525	Albany GA	86	1
537	Bangor ME	86	2
549	Watertown NY	87	1
551	Lansing MI	87	2
552	Presque Isle ME	88	1
558	Lima OH	88	2
559	Bluefield-Beckley-Oak Hill WV	89	1
565	Elmira (Corning) NY	89	2
569	Harrisonburg VA	90	1
576	Salisbury MD	90	2
581	Terre Haute IN	91	1
583	Alpena MI	91	2
588	South Bend-Elkhart IN	92	2
592	Gainesville FL	92	1
596	Zanesville OH	93	2
597	Parkersburg WV	93	1
600	Corpus Christi TX	94	1
603	Joplin MO-Pittsburg KS	94	2
606	Dothan AL	95	2
626	Victoria TX	95	1
631	Ottumwa IA-Kirksville MO	96	2
636	Harlingen-Weslaco-Brownsville-McAllen TX	96	1
647	Greenwood-Greenville MS	97	2
661	San Angelo TX	97	1
673	Columbus-Tupelo-West Point MS	98	2
702	La Crosse-Eau Claire WI	98	1
711	Meridian MS	99	1
717	Quincy IL-Hannibal MO-Keokuk IA	99	2
734	Jonesboro AR	100	2
737	Mankato MN	100	1
740	North Platte NE	101	1
745	Fairbanks AK	101	2
746	Biloxi-Gulfport MS	102	1
749	Laredo TX	102	2
760	Twin Falls ID	103	2
766	Helena MT	103	1
773	Grand Junction-Montrose CO	104	1
802	Eureka CA	104	2
868	Chico-Redding CA	105	2
10001	Glendive MT	105	1