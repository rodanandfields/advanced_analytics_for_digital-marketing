

SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SELECT top 100 CONVERT(DATE,CompletionDate) AS CompletionDate
,SUM(SubTotal) as Revenue
,COUNT(OrderID) as Transactions
,DMACode
FROM BIWarehouse.report.vOrderHeaders as oh
JOIN BIWarehouse.report.vDimCurrentAddress as addr on addr.DimAddressID = oh.DimCustomerAddressID
JOIN BIAdhoc.dbo.ZiptoDMAx3 as dma on dma.zip = addr.zip5 AND dma.DMACode <> ''
WHERE oh.Country = 'USA'
AND oh.ReturnOrderID = -1
AND DMACode = '798'
AND CONVERT(DATE, CompletionDate) BETWEEN '2019-05-28' AND '2019-10-08'
GROUP BY CONVERT(DATE, CompletionDate), DMACode




SELECT top 100 * FROM BIAdhoc.dbo.ZiptoDMAx3 WHERE DMACode = '798'

SELECT top 100 * FROM BIWarehouse.report.vDimCurrentAddress


SELECT top 100 * FROM BIWarehouse.report.vDimAccount