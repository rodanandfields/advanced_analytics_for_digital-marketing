![](google-adwords-bing-ads-2.png)  

## Overview  
In hope of optimizing its channel/program mix, Digital Marketing's Growth Initiative looks to determine whether R + F gets incremental revenue from brand keyword search ads, given our organic demand and satisfaction posture, including our already high organic search rankings for corporate brand keywords. Thus, this inter-departmental project entails exploration, study design, and final incrementality testing.  

### All Goals  
  
+ Determine whether brand keyword search ads drive incremental revenue, given our posture in other programs (e.g. brand organic search)  
+ Be able to make confident conclusions about the following customer segments through the testing period:
    - All purchasers  
    - Existing customer purchases  
    - New customer purchases  
+ Identify which, if any, DMAs exhibit an anomalous degree of incremtality, to inform campaign strategy and SEO efforts (if incrementality is low or null elsewhere or on average)

### Assumptions and Known Potential Complications 

+ Brand keyword search ads have been running nationwide for over 2 years (since June, 2016), though spend was significantly increased during our ecomm platform migration period and has stabilized at around that level since.  
+ Brand keyword search ads have used a variety of trigger/target keywords over the past year, though that has mostly stabilized since late June. However, there may have been some leakage of SEM-attributable visitors between the Atlas splash and Heirloom homepage through the migration period.
+ R + F's organic search rankings and indexed pages (for brand and non-brand search terms) dropped significantly through the platform migration period, due to our particular approach to redirects through the migration period. Most of R + F's brand search rankings returned to roughly their previous positions within two weeks of splash page removal.  

### Experimental Design  
Advanced Analytics should feel free to design this experiment as they see fit. In the meantime, Digital Marketing is positing a rough - and preliminary - framework involving the following...  

+ Target program: Corporate brand keyword search. We have other brand keyword search programs (e.g. product brand). However, their "basket of target keywords" continues to be refined.  
+ Treatment v. Control: 50/50 DMA split of the entire United States, where _treatment_ is the turning off of relevant ads in a given DMA (just like w/PLA test). Advanced Analytics should feel free to change this mix or total. However, Digital Marketing is wary of ensuring sufficient market diversity (to averange out in-DMA anomalies over test period).    
+ Ramp up period: We could potentially use our history for the "ramp up period." However, it seems wise to only go back as far as September 1, 2019, given the splash page leakage during the migration period, as well as organic search ranking resurrection not occuring till end of August.  
+ Test period: We will lean on the Advanced Analytics team to size this, given all of the segments we intend to investigate.  
+ Suggested monitoring: Since large volumes of transactions and revenue are attributable to brand keyword search campaigns, Digital Marketing is concerned with the possibility of losing substantial revenue from turning off half of the country. Thus, we will be setting up treatment DMA ecomm anomaly detection for transactions and revenue deriving from Organic Search | Direct visitors (i.e. the substitutes) through the testing period. BI/Advanced Analytics might want to do the same, from all-transactions/back-office data, for this and future such tests.  


***  

#### Exploratory Stats (where necessary)
Top level, pre-revenue...
```{r}

library(dplyr)
adwords_data <- read.csv("data/adwords_data.csv", stringsAsFactors = F)[1:6]

## filter to corporate brand sem
adwords_data <- adwords_data[intersect(grep("Brand\\|Top Level\\|Alpha|Brand\\|Top Level\\|Beta", adwords_data$Campaign), grep("Google", adwords_data$Campaign)),]

## convert dates to proper class
adwords_data$Date <- as.Date(adwords_data$Date)

print("Give us a glimpse/preview:")
glimpse(adwords_data)
print("How many days in formally grouped and labeled campaigns?")
length(unique(adwords_data$Date[grep("Brand\\|Top Level\\|Alpha|Brand\\|Top Level\\|Beta", adwords_data$Campaign)]))
print("Standard summary:")
summary(adwords_data)

```

Plot by daily and monthly intervals...
```{r fig.width=10, fig.height=8}
library(ggplot2)
library(gridExtra)

## Summarize by month (using day 1 vs. Yearmon, to help GGplot plot it more easily)
adwords_monthly <- adwords_data %>% group_by(Campaign, MonYear = as.Date(sub("..$", "01", Date))) %>% summarise(Impressions = sum(Impressions), Clicks = sum(Clicks), Cost = sum(Cost)) %>% as.data.frame()
head(adwords_monthly)

print("Monthly Totals and Averages for Complete Months, All Campaigns:")
adwords_monthly_sum <- adwords_monthly %>% group_by(MonYear) %>% summarise(Clicks = sum(Clicks), Cost = sum(Cost))
adwords_monthly_sum
adwords_full_months <- adwords_monthly[-grep("2019-07|2019-10", adwords_monthly$MonYear),] %>% summarise(Clicks = mean(Clicks), Cost = mean(Cost)) %>% as.data.frame()
daily_sums <- adwords_data %>% group_by(Date) %>% summarise(Clicks = sum(Clicks), Cost = sum(Cost))

## Plot
daily_plot_camp <- ggplot(adwords_data, aes(Date, Clicks, color = Campaign)) + geom_line()
  
monthly_plot_camp <- ggplot(adwords_monthly, aes(MonYear, Clicks, color = Campaign)) + geom_line() + geom_text(aes(label = Clicks), hjust=0, vjust=0)

daily_plot <- ggplot(daily_sums, aes(Date, Clicks)) + geom_line(color = "purple") 

monthly_plot <- ggplot(adwords_monthly_sum, aes(MonYear, Clicks)) + geom_line(color = "purple") + geom_text(aes(label = Clicks), hjust=0, vjust=0)


grid.arrange(daily_plot_camp, monthly_plot_camp, top = "Clicks by Time Interval, per Corporate Brand Campaign")
grid.arrange(daily_plot, monthly_plot, top = "Clicks by Time Interval, All Corporate Brand Campaigns")

```


#### Incrementality Tests  
They're coming...  

***  

#### Members  

+ [Tanumoy Ghosh, Director of Advanced Analytics (Lead Data Scientist)](mailto:tghosh@rodanandfields.com)  
+ [Tyler Woodward, Data Scientist at BI/Advanced Analytics](mailto:twoodward@rodanandfields.com)  
+ [Jude Calvillo, Sr. Mgr. Marketing Science & Analytics](mailto:jcalvillo@rodanandfields.com)  
+ [Nico Aguilar, Sr. Mgr. Growth Marketing](mailto:naguilar@rodanandfields.com)  
+ [Jared Fine, Conversion Optimization Manager](mailto:jfine@rodanandfields.com)  
  
  