options(scipen=999)
###################################################
# system wide options   
options(stringsAsFactors = FALSE)
options(digits=3) # number of digits to print on output
# Java Memory
options(java.parameters = "-Xmx1024m")
op <- options(digits.secs = 0)


# Time Zone
TZ = "US/Pacific"
Sys.setenv(TZ = "US/Pacific")

###################################################
# Working Directories
WD = "F:/advanced_analytics_for_digital-marketing/pla-incrementality/"
LIBLOC = "C:/Users/twoodward/Documents/R/win-library/3.4"



setwd(WD) # set working directory

###################################################


# Libraries
#suppressWarnings(require(data.table, lib.loc = LIBLOC, warn.conflicts = TRUE, quietly = TRUE))
#suppressWarnings(require(RODBC,lib.loc = LIBLOC, warn.conflicts = TRUE, quietly = TRUE)) # R SQL Server integration
#suppressWarnings(require(lazyeval,lib.loc = LIBLOC, warn.conflicts = TRUE, quietly = TRUE))
suppressWarnings(require(ggplot2, warn.conflicts = TRUE, quietly = TRUE))
suppressWarnings(require(dplyr, warn.conflicts = TRUE, quietly = TRUE))
#suppressWarnings(require(lubridate,lib.loc = LIBLOC, warn.conflicts = TRUE, quietly = TRUE))
#suppressWarnings(require(zoo,lib.loc = LIBLOC, warn.conflicts = TRUE, quietly = TRUE))
#suppressWarnings(require(stringr,lib.loc = LIBLOC, warn.conflicts = TRUE, quietly = TRUE))
suppressWarnings(require(openxlsx, warn.conflicts = TRUE, quietly = TRUE))
suppressWarnings(require(mailR, warn.conflicts = TRUE, quietly = TRUE)) # for EmailReport3 uses Apache Commons Interface
suppressWarnings(require(tictoc, warn.conflicts = TRUE, quietly = TRUE)) # 

suppressWarnings(require(RODBC, warn.conflicts = TRUE, quietly = TRUE)) # 

####This package doesn't exist here, will need to keep an eye out for that
#suppressWarnings(require(RFDSTools, warn.conflicts = FALSE, quietly = FALSE)) # R&F Data Science Library package

suppressWarnings(require(timeDate, warn.conflicts = FALSE, quietly = FALSE)) # time/date data handling package
# suppressWarnings(require(forecast,lib.loc = LIBLOC, warn.conflicts = FALSE, quietly = FALSE)) # forecasting package
# suppressWarnings(require(expsmooth,lib.loc = LIBLOC, warn.conflicts = FALSE, quietly = FALSE)) # Exponential smoothing
suppressWarnings(require(CausalImpact, warn.conflicts = FALSE, quietly = FALSE)) # Google's Causal Impact
# library("githubinstall",lib=LIBLOC)
library('stringi')

####This package is two years out of date and can't be installed for this version of R
library(GeoexperimentsResearch)

library(scales)


###################################################
Path = paste0(WD)
source(paste0(Path,'functions.R'))

###############################################################################
# Loading Data file

## Read Data Files
getOption("openxlsx.dateFormat", "mm/dd/yyyy")
options("openxlsx.dateFormat", "mm/dd/yyyy")
Path = file.path(WD,'/data')

filename1 <- file.path(Path,'rampup.xlsx')
filename2 <- file.path(Path,'testperiod.xlsx') 

ramp.up.data = read.xlsx(filename1, sheet = 2, detectDates = TRUE) 
head(ramp.up.data)

test.data = read.xlsx(filename2, sheet = 2, detectDates = TRUE) 
head(test.data)

# 
# ramp.users = read.xlsx(filename1, sheet = 1, detectDates = TRUE) 
# ramp.users$Date <- as.Date(ramp.users$Date, format = "%Y%m%d")
# ramp.users <- ramp.users %>% dplyr::select(c(Metro,Date,Users))
# 
# 
# test.users = read.xlsx(filename1, sheet = 1, detectDates = TRUE)
# test.users$Date <- as.Date(test.users$Date, format = "%Y%m%d")
# test.users <- ramp.users %>% dplyr::select(c(Metro,Date,Users))
# str(test.users)
# #seems to be quite a few NA's for Users
# left_join(ramp.up.data,ramp.users, by = c("Metro.area.(User.location)" = "Metro", "Day" = "Date")) %>% filter(!is.na(Users))

all.data <- rbind(ramp.up.data, test.data) %>% dplyr::select(-Currency)
head(all.data)
geo.metro.ref <- read.delim(file.path(Path,"geo_selection_for_test.txt") )

head(geo.metro.ref)



dbhandle <- odbcConnect(dsn = "BIAdhoc_PRDW", uid = "RFAnaltyics", pwd = "Green@123")



dma.query <- "SET NOCOUNT ON;
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

SELECT CONVERT(DATE,CompletionDate) AS CalDate
,SUM(SubTotal) as TotalSales
,COUNT(OrderID) as Transactions
,DMACode
FROM BIWarehouse.report.vOrderHeaders as oh
JOIN BIWarehouse.dim.Address as addr on addr.DimAddressID = oh.DimShippingAddressID
--JOIN BIWarehouse.report.vDimAccount as acc on acc.AccountID = oh.CustomerDemoAccountID AND acc.IsCurrent = 1
JOIN BIAdhoc.dbo.ZiptoDMAx3 as dma on dma.zip = addr.zip5 AND dma.DMACode <> ''
WHERE oh.Country = 'USA'
AND oh.ReturnOrderID = -1
AND CONVERT(DATE, CompletionDate) >= '2018-01-01' AND CONVERT(DATE,CompletionDate) <= '2019-05-31'
AND RetailAdjOrderName IN ('PC Ad-hoc')
GROUP BY CONVERT(DATE, CompletionDate), DMACode--, CustomerType"


dma.table <- sqlQuery(dbhandle, dma.query)
dma.table$CalDate <- as.Date(dma.table$CalDate)
head(dma.table)

geo.metro.sales <- dma.table %>% 
  left_join( geo.metro.ref, c("DMACode" = "geo")) %>% 
  filter(!is.na(Metro)) %>%
  mutate(geo.group = Assignment) %>% 
  dplyr::select(c(-Assignment))


head(geo.metro.sales)

str(all.data)
str(geo.metro.sales)

#calling this DF1 to avoid having to rename things down the line
DF1 <- left_join(geo.metro.sales, all.data, by = c("CalDate" = "Day", "Metro" = "Metro.area.(User.location)")) #%>% summarise(n())
DF1[is.na(DF1)] <- 0
str(DF1)
head(DF1)

head(DF1 %>% arrange(DMACode,CalDate) %>% filter(CalDate > '2019-04-01'))
head(geo.metro.sales %>% arrange(DMACode,CalDate))
head(dma.table %>% arrange(DMACode, CalDate))


#############################################################################################


CIDF <- DF1 %>% 
  #dplyr::filter(ID != 552) %>% # Presque Isle ME has too few records
  #mutate(Transactions = Clicks * CTR,
  #       RevPerTrans = ifelse(Transactions > 0, TotalSales/Transactions,0)) %>%
  mutate(cost = 0.1) %>%
  dplyr::select(date = CalDate,
                geo = DMACode, 
                Metro,
                sales = TotalSales,
                CTR,
                #cost = Cost,
                cost,
                #clicks = Clicks,
                geo.group
                #Transactions,
                #RevPerTrans
  )
head(CIDF)

# I don't remember why, but we didn't want to include these dates in the ramp up period
CIDF <-   CIDF  %>% filter(!(date <= '2019-03-31' & date >= '2019-01-28'))

CIDF %>% group_by(date) %>% summarise(daily_total = sum(sales)) %>% filter(date == '2018-05-29')

summary(CIDF$date)
# Experimental Analysis
StartDate <- '2019-04-01'
EndDate <- '2019-05-31'
obj.per <- ExperimentPeriods(c("2018-01-01", StartDate, EndDate, '2019-05-31'))


# Convert into a Geo-timeseries object
obj.gts <- GeoTimeseries(CIDF, metrics=c("sales"))


################################
#EDA
###############################
aggregate(obj.gts, by = '.weekindex')

plot(obj.gts)


geo.assign <- CIDF %>% dplyr::select(c(geo, geo.group)) %>% distinct() 

obj.ga <- GeoAssignment(geo.assign)
head(obj.ga)
###############################################


obj <- GeoExperimentData(obj.gts,
                         periods = obj.per,
                         geo.assignment = obj.ga)

head(obj)

#################################################
#More EDA
################################################
aggregate(obj, by = c('period', 'geo.group'))


#############################################
# Geo based Rgeression
################################################

result <- DoGBRROASAnalysis(obj, response = 'sales', cost = 'cost',
                            pretest.period = 0,
                            intervention.period = 1,
                            colldown.period = NULL, #Jude asked to leave off cooldown period
                            control.group = 1,
                            treatment.group = 2)

result

summary(result, level = 0.95, interval.type = "two-sided")

summary(result, threshold = 3.0)



########################################################
#Time Based Regression
######################################################

obj.tbr.roas <- DoTBRROASAnalysis(obj, response = 'sales', cost = 'cost',
                                  model = 'tbr1',
                                  pretest.period = 0,
                                  intervention.period = 1,
                                  cooldown.period = NULL,
                                  control.group = 1 ,
                                  treatment.group = 2)

obj.tbr.roas

summary(obj.tbr.roas, level = 0.95, interval.type = "two-sided")

summary(obj.tbr.roas, threshold = 3.0)

plot(obj.tbr.roas)

#####################################################

obj.tbr <- DoTBRAnalysis(obj, response = 'sales',
                         model = 'tbr1',
                         pretest.period = 0,
                         intervention.period = 1,
                         cooldown.period = 2,
                         control.group =2 ,
                         treatment.group = 1)

summary(obj.tbr)

plot(obj.tbr)  + scale_y_continuous(labels = scales::comma)

saveRDS(obj.tbr, "adhoc.rds")







####################################################


data("geoassignment")
head(geoassignment)
