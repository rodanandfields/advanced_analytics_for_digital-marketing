![](readme-banner.png)  

  
### Overview  
This repo will house the various projects Advanced Analytics and Digital Marketing collaborate on and potentially put into some form of production. Below, we link to the source code and descriptions for each current project, where each project should be fully contained within a sub-directory of this repo. Where any project is environment-dependent, a Docker compose file, or explicit environmental description, should be included in its project folder.  

***  

### Projects
+ [PLA Incrementality](pla-incrementality/)  
+ [Brand Keyword SEM Incrementality](brand-keyword-incrementality/)  
+ More coming...  

***  

### Members  

+ [Tanumoy Ghosh, Director of Advanced Analytics (Lead Data Scientist)](mailto:tghosh@rodanandfields.com)  
+ [Tyler Woodward, Data Scientist at BI/Advanced Analytics](mailto:twoodward@rodanandfields.com)  
+ [Jude Calvillo, Sr. Mgr. Marketing Science & Analytics](mailto:jcalvillo@rodanandfields.com)  
+ [Nico Aguilar, Sr. Mgr. Growth Marketing](mailto:naguilar@rodanandfields.com)  
+ [Jared Fine, Conversion Optimization Manager](mailto:jfine@rodanandfields.com)  

