![](header.png)

Overview
--------

The Growth Initiative is looking to determine whether order type mix has
significantly changed (or at least detectably changed) since deploying
paid social campaigns targeting presumably net new prospects. [A
Chi-Square
test](https://www.statisticssolutions.com/chi-square-goodness-of-fit-test/),
for *goodness of fit*, upon the order type mix pre and post-paid social
campaign deployment should be able to confidently tell us this. However,
it must be done on EDW/backend data, as ‘order type’ tracking is
currently down in Atlas R2 web tracking.
